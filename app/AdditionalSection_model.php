<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdditionalSection_model extends Model
{
    protected $table = 'additional_sections';
}
