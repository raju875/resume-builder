<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Degree_model extends Model
{
    protected $table = 'degrees';
}
