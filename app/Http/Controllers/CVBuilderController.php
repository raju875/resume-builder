<?php

namespace App\Http\Controllers;

use App\AdditionalSection_model;
use App\Degree_model;
use App\Education_model;
use App\Experience_model;
use App\Hobby_model;
use App\Language_model;
use App\PersonalInfo_model;
use App\Personality_model;
use App\Profile_model;
use App\Result_model;
use App\Skill_model;
use App\SocialMedia_model;
use App\SoftwareSkill_model;
use Illuminate\Http\Request;
use Validator;
use Image;
use PDF;

class CVBuilderController extends Controller
{

    public function cvBuilder()
    {
        return view("cv.cv-builder");
    }

    public function cvList()
    {
        return view("cv.list-of-cv");
    }

    public function myCv($infoId)
    {
        return view("cv.my-cv",[
            'infoId' => $infoId
        ]);
    }

    public function resumeDownload($infoId) {

        $pdf = PDF::loadView('cv.download-cv',[
            'infoId' => $infoId
        ]);
       return $pdf->stream('resume.pdf');
    }

    public function markSheetDownload($infoId) {

        $pdf = PDF::loadView('cv.download-mark-sheet',[
            'infoId' => $infoId
        ]);
       return $pdf->stream('mark-sheet.pdf');
    }

    public function resumeDelete($id) {
        PersonalInfo_model::find($id)->delete();
        Profile_model::where('personal_info_id',$id)->delete();
        SocialMedia_model::where('personal_info_id',$id)->delete();
        Education_model::where('personal_info_id',$id)->delete();
        Result_model::where('personal_info_id',$id)->delete();
        Experience_model::where('personal_info_id',$id)->delete();
        Skill_model::where('personal_info_id',$id)->delete();
        SoftwareSkill_model::where('personal_info_id',$id)->delete();
        Personality_model::where('personal_info_id',$id)->delete();
        Language_model::where('personal_info_id',$id)->delete();
        Hobby_model::where('personal_info_id',$id)->delete();
        AdditionalSection_model::where('personal_info_id',$id)->delete();

        return redirect('/list-of-cv')->with('message','Successfully item delete');
    }


    public function cvBuilderPost(Request $request)
    {
        $rulesPersonalInfo = [];
        $rulesProfile = [];
        $rulesSocialMedia = [];
        $rulesEducation = [];
        $rulesResult = [];
        $rulesExperience = [];
        $rulesLanguage = [];
        $rulesPersonality = [];
        $rulesHobby = [];
        $rulesAdditionalSection = [];

        $rulesPersonalInfo['first_name'] = 'required|max:255';
        $rulesPersonalInfo['last_name'] = 'required|max:255';
        $rulesPersonalInfo['gender'] = 'required';
        $rulesPersonalInfo['date_of_birth'] = 'required';
        $rulesPersonalInfo['email'] =  'required|email|max:255';
        $rulesPersonalInfo['phone'] = 'required|size:11|regex:/(01)[0-9]{9}/';
        $rulesPersonalInfo['address'] = 'required';
        $rulesPersonalInfo['zip_code'] = 'required';
//        $rulesPersonalInfo['image'] = 'required';

        // *************************** try to save image ******************************
//        $image = $request->file('image');
//        $imageNewName = $image->getClientOriginalName();
//        $directory = 'cv/profile-images/';
//        $imageUrl = $directory . $imageNewName;
//        Image::make($image)->save($imageUrl);
        // *************************** try to save image *******************************





        $validatorPersonalInfo = Validator::make($request->all(), $rulesPersonalInfo);

        if ($validatorPersonalInfo->passes()) {
            // if personal info validation is ok then validation check to profile

            $rulesProfile['job_title'] = 'required|max:255';
            $rulesProfile['summary'] = 'required';

            $validatorProfile = Validator::make($request->all(), $rulesProfile);

            if ($validatorProfile->passes()) {
                foreach($request->input('social_media_link') as $key => $valueSocialMedia) {
                    $rulesSocialMedia["social_media.{$key}"] = 'required';
                    $rulesSocialMedia["social_media_link.{$key}"] = 'required';
                }
                $validatorSocialMedia = Validator::make($request->all(), $rulesSocialMedia);

                if ($validatorSocialMedia->passes()) {
                    // if social media validation is ok then validation check to education

                    foreach($request->input('major') as $key => $valueEducation) {
                        $rulesEducation["institution_name.{$key}"] = 'required';
                        $rulesEducation["major.{$key}"] = 'required';
                        $rulesEducation["start_date.{$key}"] = 'required|date';
                        $rulesEducation["end_date.{$key}"] = 'nullable|date';
                        $rulesEducation["cgpa.{$key}"] = 'required|numeric|min:1|max:5';
                    }
                    $validatorEducation = Validator::make($request->all(), $rulesEducation);

                    if ($validatorEducation->passes()) {
                        // if education validation is ok then validation check to result

                        if ($request->degree) {
                            $rulesResult['degree'] = 'required|string';

                            foreach($request->input('course_title') as $key => $valueResult) {
                                $rulesResult["course_code.{$key}"] = 'required|alpha_num';
                                $rulesResult["course_title.{$key}"] ='required|regex:/^[a-zA-Z]+$/u|max:255';
                                $rulesResult["grade.{$key}"] = 'required|string';
                                $rulesResult["grade_point.{$key}"] = 'required|numeric|min:1|max:5';
                            }
                            $validatorResult = Validator::make($request->all(), $rulesResult);
                        } else {
                            $rulesResult['degree'] = 'nullable';

                            foreach($request->input('course_title') as $key => $valueResult) {
                                $rulesResult["course_code.{$key}"] = 'nullable';
                                $rulesResult["course_title.{$key}"] ='nullable';
                                $rulesResult["grade.{$key}"] = 'nullable';
                                $rulesResult["grade_point.{$key}"] = 'nullable';
                            }
                            $validatorResult = Validator::make($request->all(), $rulesResult);
                        }

                        if ($validatorResult->passes()) {
                            // if result validation is ok then validation check to experience


                            foreach($request->input('experience_designation') as $key => $valueExperience) {
                                $rulesExperience["experience_start_date.{$key}"] = 'nullable';
                                $rulesExperience["experience_institution_name.{$key}"] = 'nullable';
                                $rulesExperience["experience_designation.{$key}"] = 'nullable';
                                $rulesExperience["experience_about_job.{$key}"] = 'nullable';
                            }
                            $validatorExperience = Validator::make($request->all(), $rulesExperience);

                            if($validatorExperience->passes()) {
                                // if experience validation is ok then validation check to personality

                                foreach($request->input('personality') as $key => $valuePersonality) {
                                    $rulesPersonality["personality.{$key}"] = 'required';
                                }
                                $validatorPersonality = Validator::make($request->all(), $rulesPersonality);



                                if ($validatorPersonality->passes()) {
                                    // if language validation is ok then validation check to language

                                    foreach($request->input('language') as $key => $valueLanguage) {
                                        $rulesLanguage["language.{$key}"] = 'required';
                                    }
                                    $validatorLanguage = Validator::make($request->all(), $rulesLanguage);

                                    if ($validatorLanguage->passes()) {
                                        // if personality validation is ok then validation check to hobby

                                        foreach($request->input('hobby') as $key => $valueHobby) {
                                            $rulesHobby["hobby.{$key}"] = 'required';
                                        }
                                        $validatorHobby = Validator::make($request->all(), $rulesHobby);

                                        if( $validatorHobby->passes()) {
                                            // if hobby validation is ok then validation check to additional section
                                            foreach($request->input('subject') as $key => $valueSubject) {
                                                $rulesAdditionalSection["subject.{$key}"] = 'nullable';
                                                $rulesAdditionalSection["text.{$key}"] = 'nullable';
                                            }
                                            $validatorAdditionalSection = Validator::make($request->all(), $rulesAdditionalSection);

                                            if ($validatorAdditionalSection->passes()) {
                                                // all the validation is complete and data save to database

                                                //+++++++++ save personal info date  +++++++++++
                                                $personalInfo = new PersonalInfo_model();
                                                $personalInfo->first_name = $request->first_name;
                                                $personalInfo->last_name = $request->last_name;
                                                $personalInfo->gender = $request->gender;
                                                $personalInfo->date_of_birth = $request->date_of_birth;
                                                $personalInfo->email = $request->email;
                                                $personalInfo->phone = $request->phone;
                                                $personalInfo->address = $request->address;
                                                $personalInfo->zip_code = $request->zip_code;
                                                $personalInfo->image = $request->image;

                                                $personalInfo->save();


                                                //++++++++++ save profile data +++++++++++++
                                                $profileInfo = new Profile_model();
                                                $profileInfo->personal_info_id = $personalInfo->id;
                                                $profileInfo->job_title = $request->job_title;
                                                $profileInfo->summary = $request->summary;
                                                $profileInfo->save();



                                                //++++++++++ save social media data +++++++++++++
                                                $social_media = $request->social_media;
                                                $social_media_link = $request->social_media_link;

                                                for($count = 0; $count < count($social_media_link); $count++)
                                                {
                                                    $socialMediaData = array(
                                                        'personal_info_id' =>$personalInfo->id,
                                                        'social_media' => $social_media[$count],
                                                        'social_media_link'  => $social_media_link[$count]
                                                    );
                                                    $insertSocialMediaData[] = $socialMediaData;
                                                }

                                                SocialMedia_model::insert($insertSocialMediaData);


                                                // ++++++++++++++++ save education  ++++++++++++++++++
                                                $institution_name = $request->institution_name;
                                                $major = $request->major;
                                                $start_date = $request->start_date;
                                                $end_date = $request->end_date;
                                                $cgpa = $request->cgpa;

                                                for($count = 0; $count < count($major); $count++)
                                                {
                                                    $education = array(
                                                        'personal_info_id' =>$personalInfo->id,
                                                        'institution_name' =>$institution_name[$count],
                                                        'major' => $major[$count],
                                                        'start_date'  => $start_date[$count],
                                                        'end_date'  => $end_date[$count],
                                                        'cgpa'  => $cgpa[$count]
                                                    );
                                                    $insertEducationData[] = $education;
                                                }

                                                Education_model::insert($insertEducationData);

                                                // ++++++++++++++++ save skill  ++++++++++++++++++

                                                $skills = $request->skill;
                                                for ($count=0; $count < count($skills); $count++ ) {
                                                    $skillInfo = new Skill_model();
                                                    $skillInfo->personal_info_id = $personalInfo->id;
                                                    $skillInfo->skill = $skills[$count];
                                                    $skillInfo->save();
                                                }


                                                // ++++++++++++++++ save software skill  ++++++++++++++++++

                                                $softwareSkills = $request->software_skill;
                                                for ($count=0; $count < count($softwareSkills); $count++ ) {
                                                    $softwareSkillInfo = new SoftwareSkill_model();
                                                    $softwareSkillInfo->personal_info_id = $personalInfo->id;
                                                    $softwareSkillInfo->software_skill = $softwareSkills[$count];
                                                    $softwareSkillInfo->save();
                                                }


                                                // ++++++++++++++++ save personality  ++++++++++++++++++

                                                $personalityInfo = $request->personality;
                                                for ($count=0; $count < count($personalityInfo); $count++ ) {
                                                    $personality = new Personality_model();
                                                    $personality->personal_info_id = $personalInfo->id;
                                                    $personality->personality = $personalityInfo[$count];
                                                    $personality->save();
                                                }


                                                // ++++++++++++++++ save language  ++++++++++++++++++

                                                $language = $request->language;
                                                $languageScore = $request->score;
                                                for ($count=0; $count < count($language); $count++ ) {
                                                    $languageInfo = new Language_model();
                                                    $languageInfo->personal_info_id = $personalInfo->id;
                                                    $languageInfo->language = $language[$count];
                                                    $languageInfo->score = $languageScore[$count];
                                                    $languageInfo->save();
                                                }


                                                // ++++++++++++++++ save language  ++++++++++++++++++

                                                $hobby = $request->hobby;
                                                for ($count=0; $count < count($hobby); $count++ ) {
                                                    $hobbyInfo = new Hobby_model();
                                                    $hobbyInfo->personal_info_id = $personalInfo->id;
                                                    $hobbyInfo->hobby = $hobby[$count];
                                                    $hobbyInfo->save();
                                                }


                                                // ++++++++++++++++ save experience  ++++++++++++++++++

                                                $experience_start_date = $request->experience_start_date;
                                                $experience_end_date = $request->experience_end_date;
                                                $experience_institution_name = $request->experience_institution_name;
                                                $experience_designation = $request->experience_designation;
                                                $experience_about_job = $request->experience_about_job;

                                                for($count = 0; $count < count($experience_institution_name); $count++)
                                                {
                                                    $experience = array(
                                                        'personal_info_id' =>$personalInfo->id,
                                                        'experience_start_date' =>$experience_start_date[$count],
                                                        'experience_end_date' => $experience_end_date[$count],
                                                        'experience_institution_name'  => $experience_institution_name[$count],
                                                        'experience_designation'  => $experience_designation[$count],
                                                        'experience_about_job'  => $experience_about_job[$count]
                                                    );
                                                    $insertExperienceData[] = $experience;
                                                }

                                                Experience_model::insert($insertExperienceData);



//                                        for ($count=0; $count < ($experience_designation); $count++) {
//                                            $experienceInfo = new Experience_model();
//                                            $experienceInfo->personal_info_id = $personalInfo->id;
//                                            $experienceInfo->experience_start_date = $experience_start_date[$count];
//                                            $experienceInfo->experience_end_date = $experience_end_date[$count];
//                                            $experienceInfo->experience_institution_name = $experience_institution_name[$count];
//                                            $experienceInfo->experience_designation = $experience_designation[$count];
//                                            $experienceInfo->experience_about_job = $experience_about_job[$count];
//
//                                            $experienceInfo->save();
//                                        }


                                                // ++++++++++++++++ save result  ++++++++++++++++++
                                                $course_code = $request->course_code;
                                                $course_title = $request->course_title;
                                                $grade = $request->grade;
                                                $grade_point = $request->grade_point;

                                                $degree = new Degree_model();
                                                $degree->personal_info_id = $personalInfo->id;
                                                $degree->degree = $request->degree;
                                                $degree->save();


                                                for($count = 0; $count < count($course_code); $count++)
                                                {
                                                    $result = array(
                                                        'personal_info_id' =>$personalInfo->id,
                                                        'degree_id' =>$degree->id,
                                                        'course_code' => $course_code[$count],
                                                        'course_title'  => $course_title[$count],
                                                        'grade'  => $grade[$count],
                                                        'grade_point'  => $grade_point[$count]
                                                    );
                                                    $insertResultData[] = $result;
                                                }
                                                Result_model::insert($insertResultData);


                                                // ++++++++++++++++ save additional section  ++++++++++++++++++

                                                $subject = $request->subject;
                                                $text = $request->text;

                                                for($count = 0; $count < count($subject); $count++)
                                                {
                                                    $additionalSection = array(
                                                        'personal_info_id' =>$personalInfo->id,
                                                        'subject' =>$subject[$count],
                                                        'text' =>$text[$count],
                                                    );
                                                    $insertAdditionalSectionData[] = $additionalSection;
                                                }

                                                AdditionalSection_model::insert($insertAdditionalSectionData);

                                                // **************** all the data have been saved *****************

                                                return response()->json(['success'=>'done']);
                                            }
                                            return response()->json(['error'=>$validatorAdditionalSection->errors()->all()]);
                                        }
                                        return response()->json(['error'=>$validatorHobby->errors()->all()]);
                                    }
                                    return response()->json(['error'=>$validatorLanguage->errors()->all()]);
                                }

                                return response()->json(['error'=>$validatorPersonality->errors()->all()]);
                            }
                            return response()->json(['error'=>$validatorExperience->errors()->all()]);
                        }
                        return response()->json(['error'=>$validatorResult->errors()->all()]);
                    }
                    return response()->json(['error'=>$validatorEducation->errors()->all()]);
                }
                return response()->json(['error'=>$validatorSocialMedia->errors()->all()]);
            }
            return response()->json(['error'=>$validatorProfile->errors()->all()]);
        }
        return response()->json(['error'=>$validatorPersonalInfo->errors()->all()]);
    }

}
