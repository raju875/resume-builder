<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personality_model extends Model
{
    protected $table = 'personalities';
}
