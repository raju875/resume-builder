<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill_model extends Model
{
    protected $table = 'skills';
}
