<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialMedia_model extends Model
{
    protected $table = 'social_medias';
}
