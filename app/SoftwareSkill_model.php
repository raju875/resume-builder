<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoftwareSkill_model extends Model
{
   protected $table = 'software_skills';
}
