@extends('cv.master')

@section('title')
    <title>CV Builder</title>
    @endsection

@section('content')
    <div class="container mt-5">
        <div class="row">

{{--            <!--  My CV-->--}}
{{--            <div class="coi-lg-7 col-md-7 col-sm-7 col-xs-6">--}}

{{--                <div class="row">--}}
{{--                    <div class="col-md-3 mt-3">--}}
{{--                        <img src="{{ asset('cv/assets/') }} /images/image.jpg" class="img-fluid" alt="Image" style="width:150px; height: 120px; border-radius:50%">--}}
{{--                    </div>--}}
{{--                    <div class="col-md-9">--}}
{{--                        <h5>Name : Al Amin Raju</h5>--}}
{{--                        <h6>Designation : Developer</h6>--}}
{{--                        <p class="text-justify">Description : As of 2017, text messages are used by youth and adults for personal, family, business and social purposes. Governmental and non-governmental organizations use text messaging for communication between colleagues. In the 2010s, the sending of short informal messages has become an accepted part of many cultures, as happened earlier with emailing.[1] This makes texting a quick and easy way to communicate with friends, family and colleagues, including in contexts where a call would be impolite or inappropriate (e.g., calling very late at night or when one knows the other person is busy with family or work activities). Like e-mail and voicemail, and unlike calls (in which the caller hopes to speak directly with the recipient), texting does not require the caller and recipient to both be free at the same moment; this permits communication even between busy individuals. Text messages can also be used to interact with automated systems, for example, to order products or services from </p>--}}

{{--                        <!--    Location/Date-of-birth/Phone/Email-->--}}
{{--                        <div class="container-fluid">--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-6"><i class="fas fa-home"></i></div>--}}
{{--                                <div class="col-md-6"><i class="fas fa-birthday-cake"></i></div>--}}
{{--                            </div>--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-6"><i class="fas fa-mobile-alt"></i></div>--}}
{{--                                <div class="col-md-6"><i class="fas fa-envelope-open-text"></i></div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <!--    Location/Date-of-birth/Phone/Email-->--}}
{{--                    </div>--}}

{{--                    <!--    Social Media-->--}}
{{--                    <div class="container">--}}
{{--                        <h4>Social Media</h4>--}}
{{--                        <hr>--}}
{{--                        <div class="container-fluid">--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-6"><i class="fab fa-facebook-f"></i></div>--}}
{{--                                <div class="col-md-6"><i class="fab fa-linkedin-in"></i></div>--}}
{{--                            </div>--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-6"><i class="fab fa-github"></i></div>--}}
{{--                                <div class="col-md-6"><i class="fab fa-skype"></i></div>--}}
{{--                            </div>--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-6"><i class="fab fa-google-plus-g"></i></div>--}}
{{--                                <div class="col-md-6"><i class="fab fa-twitter"></i></div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!--    Social Media-->--}}

{{--                    <!--    Education-->--}}
{{--                    <div class="container">--}}
{{--                        <h4>Education</h4>--}}
{{--                        <hr>--}}
{{--                        <div class="container-fluid">--}}
{{--                            <table class="table">--}}
{{--                                <thead>--}}
{{--                                <tr>--}}
{{--                                    <th scope="col">#</th>--}}
{{--                                    <th scope="col">Institution Name</th>--}}
{{--                                    <th scope="col">Group</th>--}}
{{--                                    <th scope="col">CGP-A</th>--}}
{{--                                    <th scope="col">Passing Year</th>--}}
{{--                                </tr>--}}
{{--                                </thead>--}}
{{--                                <tbody>--}}
{{--                                <tr>--}}
{{--                                    <th scope="row">1</th>--}}
{{--                                    <td>Eastern University</td>--}}
{{--                                    <td>B.Sc</td>--}}
{{--                                    <td>3.41</td>--}}
{{--                                    <td>2019</td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <th scope="row">1</th>--}}
{{--                                    <td>DN College & University</td>--}}
{{--                                    <td>Science</td>--}}
{{--                                    <td>4.70</td>--}}
{{--                                    <td>2012</td>--}}
{{--                                </tr>--}}
{{--                                </tbody>--}}
{{--                            </table>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!--    Education-->--}}

{{--                    <!--    Experience-->--}}
{{--                    <div class="container">--}}
{{--                        <h4>Experience</h4>--}}
{{--                        <hr>--}}
{{--                        <div class="container-fluid">--}}
{{--                            <h5>Institution Name</h5>--}}
{{--                            <h6>Designation</h6>--}}
{{--                            <h6>Working Year</h6>--}}
{{--                            <h6>About the job</h6>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!--    Experience-->--}}


{{--                    <div class="container">--}}
{{--                        <div class="row">--}}
{{--                            <!--     Skill-->--}}
{{--                            <div class="col-md-6">--}}
{{--                                <div class="container">--}}
{{--                                    <h2>Skill</h2>--}}
{{--                                    <hr>--}}
{{--                                    <ul class="list-group">--}}
{{--                                        <li>MS Word</li>--}}
{{--                                        <li>PHP Storm</li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!--     Skill-->--}}

{{--                            <!--    Software Skill-->--}}
{{--                            <div class="col-md-6">--}}
{{--                                <div class="container">--}}
{{--                                    <h2>Software Skill</h2>--}}
{{--                                    <hr>--}}
{{--                                    <ul class="list-group">--}}
{{--                                        <li>MS Word</li>--}}
{{--                                        <li>PHP Storm</li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!--    Software Skill-->--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="container">--}}
{{--                        <div class="row">--}}
{{--                            <!--     Personality-->--}}
{{--                            <div class="col-md-6">--}}
{{--                                <div class="container">--}}
{{--                                    <h2>Personality</h2>--}}
{{--                                    <hr>--}}
{{--                                    <div class="badge badge-secondary text-wrap">Commutative</div>--}}
{{--                                    <div class="badge badge-secondary text-wrap">Commutative</div>--}}
{{--                                    <div class="badge badge-secondary text-wrap">Commutative</div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!--     Personality-->--}}

{{--                            <!--    Language-->--}}
{{--                            <div class="col-md-6">--}}
{{--                                <div class="container">--}}
{{--                                    <h2>Language</h2>--}}
{{--                                    <hr>--}}
{{--                                    <p>Language1</p>--}}
{{--                                    <span class="dotDark"></span>--}}
{{--                                    <span class="dotDark"></span>--}}
{{--                                    <span class="dotDark"></span>--}}
{{--                                    <span class="dot"></span>--}}
{{--                                    <span class="dot"></span>--}}
{{--                                    <p>Language2</p>--}}
{{--                                    <span class="dotDark"></span>--}}
{{--                                    <span class="dotDark"></span>--}}
{{--                                    <span class="dot"></span>--}}
{{--                                    <span class="dot"></span>--}}
{{--                                    <span class="dot"></span>--}}
{{--                                    <p>Language3</p>--}}
{{--                                    <span class="dotDark"></span>--}}
{{--                                    <span class="dotDark"></span>--}}
{{--                                    <span class="dotDark"></span>--}}
{{--                                    <span class="dotDark"></span>--}}
{{--                                    <span class="dot"></span>--}}
{{--                                    <p>Language4</p>--}}
{{--                                    <span class="dotDark"></span>--}}
{{--                                    <span class="dotDark"></span>--}}
{{--                                    <span class="dot"></span>--}}
{{--                                    <span class="dot"></span>--}}
{{--                                    <span class="dot"></span>--}}
{{--                                    <span class="dot"></span>--}}
{{--                                    <p>Language5</p>--}}
{{--                                    <span class="dotDark"></span>--}}
{{--                                    <span class="dot"></span>--}}
{{--                                    <span class="dot"></span>--}}
{{--                                    <span class="dot"></span>--}}
{{--                                    <span class="dot"></span>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!--    Language-->--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <!--    Hobby-->--}}
{{--                    <div class="container">--}}
{{--                        <h4>Hobby</h4>--}}
{{--                        <hr>--}}
{{--                        <div class="container-fluid">--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-2"><i class="fab fa-facebook-f"></i>--}}
{{--                                    <p id="insertHobbies"></p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!--    Hobby-->--}}


{{--                </div>--}}
{{--            </div>--}}
{{--            <!--  My CV-->--}}

            <!--  My CV Form-->
            <div class="col-12">
                <form name="add_name" id="add_name" enctype="multipart/form-data">

                    <!--    Personal Information-->
                    <a class="btn btn-block  btn-primary" data-toggle="collapse" href="#personalInformation" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fas fa-user-alt"></i>  Personal Information <span style="color: red;">*</span>
                    </a>
                    <div class="collapse" id="personalInformation">
                        <div class="card card-body">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>First Name</label>
                                    <input type="type" class="form-control" required name="first_name" placeholder="First Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Last Name</label>
                                    <input type="type" class="form-control" required name="last_name" placeholder="Last Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Gender</label>
                                <select class="form-control" required name="gender">
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                    <option value="3">Others</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Date Of Birth</label>
                                <input type="date" class="form-control" required name="date_of_birth">
                            </div>
                            <h5><i class="fal fa-id-card"></i>  Contact</h5>
                            <hr>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" required name="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="number" class="form-control" required name="phone" placeholder="Phone No">
                            </div>

                            <h5><i class="fal fa-home-lg-alt"></i> Address</h5>
                            <hr>
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control" name="address" required placeholder="Your Address">
                            </div>
                            <div class="form-group">
                                <label>Zip Code</label>
                                <input type="number" class="form-control" name="zip_code" required placeholder="Zip Code">
                            </div>

                            <h5>Add Photo</h5>
                            <hr>
                            <div class="form-group">
                                <label>Add/Change Photo</label>
                                <input type="file" name="image" class="form-control" onchange="previewImage(event)" >
                                <div class="mt-4 ml-5" ><img src="{{asset('cv/profile-images/blankimage.jpg')}}" width="150" height="110" id="image-field" > </div>

                            </div>
                        </div>
                    </div>
                    <!--    Personal Information-->

                    <!--      Profile-->
                    <a class="btn btn-block mt-2 btn-primary" data-toggle="collapse" href="#profile" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fas fa-home-lg-alt"></i>  Profile <span style="color: red;">*</span>
                    </a>
                    <div class="collapse" id="profile">
                        <div class="card card-body">
                            <div class="input_fields_profile">
                                <div class="form-group">
                                    <label>Job Title</label>
                                    <input type="text" class="form-control" id="job_title" required name="job_title">
                                </div>
                                <div class="form-group">
                                    <label>Summary</label>
                                    <input type="text" class="form-control" id="summary" required name="summary">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--     Profile-->

                    <!--      Social Media-->
                    <a class="btn btn-block mt-2  btn-primary" data-toggle="collapse" href="#socialMedia" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fas fa-home-lg-alt"></i>  Social Media <span style="color: red;">*</span>
                    </a>
                    <div class="collapse" id="socialMedia">
                        <div class="card card-body">
                            <div class="input_fields_social_media row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Social Media</label>
                                        <select class="form-control" required name="social_media[]">
                                            <option value=""></option>
                                            <option value="facebook">Facebook</option>
                                            <option value="twitter">Twitter</option>
                                            <option value="linkdin">Linkdin</option>
                                            <option value="google_plus">Google+</option>
                                            <option value="skype">Skype</option>
                                            <option value="github">Github</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div>
                                        <label>Social Media Link</label>
                                        <input type="text" class="form-control" required name="social_media_link[]">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="add_social_media_field col-md-3">Add More</button>
                    </div>
                    <!--      Social Media-->

                    <!--      Education-->
                    <a class="btn btn-block mt-2  btn-primary" data-toggle="collapse" href="#education" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fas fa-home-lg-alt"></i>  Education <span style="color: red;">*</span>
                    </a>
                    <div class="collapse" id="education">
                        <div class="card card-body">
                            <div class="input_fields_education">
                                <div class="form-group">
                                    <label>Institution Name</label>
                                    <input type="text" class="form-control" required name="institution_name[]">
                                </div>
                                <div class="form-group">
                                    <label>Major</label>
                                    <input type="text" class="form-control" required name="major[]">
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Start Date</label>
                                        <input type="date" class="form-control" required name="start_date[]">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>End Date</label>
                                        <input type="date" class="form-control" name="end_date[]">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>CGP-A</label>
                                    <input type="number" class="form-control" max="4" required name="cgpa[]">
                                </div>
                            </div>
                        </div>
                        <button class="add_education_field col-md-3">Add More</button>
                    </div>
                    <!--      Education-->


                    <!--    Result-->
                    <a class="btn btn-block mt-2  btn-primary" data-toggle="collapse" href="#result" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fas fa-home-lg-alt"></i>  Result
                    </a>
                    <div class="collapse" id="result">
                        <div class="card card-body">
                            <div class="input_fields_result">
                                <div class="form-group">
                                    <label>Degree</label>
                                    <input type="text" class="form-control" required name="degree">
                                </div>
                                <div class="form-group">
                                    <label>Course Code</label>
                                    <input type="text" class="form-control" required name="course_code[]">
                                </div>
                                <div class="form-group">
                                    <label>Course Title</label>
                                    <input type="text" class="form-control" required name="course_title[]">
                                </div>
                                <div class="form-group">
                                    <label>Grade</label>
                                    <input type="text" class="form-control" required name="grade[]">
                                </div>
                                <div class="form-group">
                                    <label>Grade Point</label>
                                    <input type="number" class="form-control" required max="5" name="grade_point[]">
                                </div>
                            </div>
                        </div>
                        <button class="add_result_field col-md-3">Add More</button>
                    </div>
                    <!--    Result-->

                    <!--    Experience-->
                    <a class="btn btn-block mt-2  btn-primary" data-toggle="collapse" href="#experience" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fas fa-home-lg-alt"></i>  Experience
                    </a>
                    <div class="collapse" id="experience">
                        <div class="card card-body">
                            <div class="input_fields_experience">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Start Date</label>
                                        <input type="date" class="form-control" required name="experience_start_date[]">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>End Date</label>
                                        <input type="date" class="form-control" name="experience_end_date[]">
                                    </div>
                                    <div class="form-group">
                                        <input type="checkbox">present
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Institution Name</label>
                                    <input type="text" class="form-control" required name="experience_institution_name[]">
                                </div>
                                <div class="form-group">
                                    <label>Designation</label>
                                    <input type="text" class="form-control" required name="experience_designation[]">
                                </div>
                                <div class="form-group">
                                    <label>About Job</label>
                                    <input type="text" class="form-control" required name="experience_about_job[]">
                                </div>
                            </div>
                        </div>
                        <button class="add_experience_field col-md-3">Add More</button>
                    </div>
                    <!--    Experience-->

                    <!--      Skill-->
                    <a class=" btn-block mt-2 btn btn-primary" data-toggle="collapse" href="#skill" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fas fa-home-lg-alt"></i>  Skill <span style="color: red;">*</span>
                    </a>
                    <div class="collapse" id="skill">
                        <div class="card card-body">
                            <div class="input_fields_skill">
                                <div class="form-group">
                                    <label>Skill</label>
                                    <select class="form-control" required name="skill[]">
                                        <option value="html">HTML</option>
                                        <option value="css">CSS</option>
                                        <option value="php">PHP</option>
                                        <option value="js">JavaScript</option>
                                        <option value="laravel">Laravel</option>
                                        <option value="vue_js">Vue.js</option>
                                        <option value="wordpress">WordPress</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button class="add_skill_field col-md-3">Add More</button>
                    </div>
                    <!--      Skill-->


                    <!--     Software Skill-->
                    <a class="btn btn-block mt-2  btn-primary" data-toggle="collapse" href="#softwareSkill" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fas fa-home-lg-alt"></i>  Software Skill <span style="color: red;">*</span>
                    </a>
                    <div class="collapse" id="softwareSkill">
                        <div class="card card-body">
                            <div class="input_fields_software_skill">
                                <div class="form-group">
                                    <label>Software Skill</label>
                                    <select class="form-control" required name="software_skill[]">
                                        <option value="adobe_photoshop">Adobe Photoshop</option>
                                        <option value="adobe_illustrator">Adobe Illustrator</option>
                                        <option value="ms_word">MS Word</option>
                                        <option value="power_point">PowerPoint</option>
                                        <option value="xampp">XAMPP</option>
                                        <option value="php_storm">PHP Storm</option>
                                        <option value="web_storm">Web Storm</option>
                                        <option value="netbeans">Netbeans</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button class="add_software_skill_field col-md-3">Add More</button>
                    </div>
                    <!--      Software Skill-->


                    <!--     Personality-->
                    <a class="btn  btn-block mt-2 btn-primary" data-toggle="collapse" href="#personality" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fas fa-home-lg-alt"></i>  Personality <span style="color: red;">*</span>
                    </a>
                    <div class="collapse" id="personality">
                        <div class="card card-body">
                            <div class="input_fields_personality">
                                <div class="form-group">
                                    <label>Personality</label>
                                    <input type="text" class="form-control" required name="personality[]">
                                </div>
                            </div>
                        </div>
                        <button class="add_personality_field col-md-3">Add More</button>
                    </div>
                    <!--      Personality-->


                    <!--     Language-->
                    <a class="btn btn-block mt-2  btn-primary" data-toggle="collapse" href="#language" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fas fa-home-lg-alt"></i>  Language <span style="color: red;">*</span>
                    </a>
                    <div class="collapse" id="language">
                        <div class="card card-body">
                            <div class="input_fields_language">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Language</label>
                                        <input type="text" class="form-control" required name="language[]">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Score</label>
                                        <select class="form-control" name="score[]">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="add_language_field col-md-3">Add More</button>
                    </div>
                    <!--      Language-->

                    <!--     Hobby-->
                    <a class="btn btn-block mt-2  btn-primary" data-toggle="collapse" href="#hobby" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fas fa-home-lg-alt"></i>  Hobbies <span style="color: red;">*</span>
                    </a>
                    <div class="collapse" id="hobby">
                        <div class="card card-body">
                            <div class="input_fields_hobby">
                                <div class="form-group">
                                    <label>Hobby</label>
                                    <input type="text" class="form-control" id="hobbies" required name="hobby[]">
                                </div>
                            </div>
                        </div>
                        <button class="add_hobby_field col-md-3">Add More</button>
                    </div>
                    <!--      Hobby-->


                    <!--     Additional Section-->
                    <a class="btn btn-block mt-2  btn-primary" data-toggle="collapse" href="#additionalSection" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fas fa-home-lg-alt"></i>  Additional Section
                    </a>
                    <div class="collapse" id="additionalSection">
                        <div class="card card-body">
                            <div class="input_fields_additional_section">
                                <div class="form-group">
                                    <label>Subject</label>
                                    <input type="text" class="form-control" id="subject" required name="subject[]">
                                </div>
                                <div class="form-group">
                                    <label>Text</label>
                                    <input type="text" class="form-control" id="text" required name="text[]">
                                </div>
                            </div>
                        </div>
                        <button class="add_additional_section_field col-md-3">Add More</button>
                    </div>
                    <!--      Additional Section-->


                    <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul></ul>
                    </div>
                    <div class="alert alert-success print-success-msg" style="display:none">
                        <ul></ul>
                    </div>

                    <input type="button" name="submit" id="submit" class="btn btn-block mt-2  btn-primary" value="Submit" />

                </form>
            </div>
            <!--  My CV Form-->
        </div>
    </div>


    <script type="text/javascript">

        // preview image before upload
        function previewImage(event) {
            var reader = new FileReader();
            var imageField = document.getElementById("image-field");
            reader.onload = function () {
                if (reader.readyState==2 ) {
                    imageField.src = reader.result;
                }
            }
            reader.readAsDataURL(event.target.files[0]);
        }
        // preview image before upload


        // social media
        $(document).ready(function() {
            var max_fields      = 100; //maximum input boxes allowed
            var wrapper   		= $(".input_fields_social_media"); //Fields wrapper
            var add_button      = $(".add_social_media_field"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div class="row"><div class="col-md-4"><div class="form-group"><label>Social Media</label><select class="form-control" required name="social_media[]"><option value="facebook">Facebook</option><option value="twitter">Twitter</option><option value="linkdin">Linkdin</option><option value="google_plus">Google+</option><option value="skype">Skype</option></select></div></div>' +
                        '<div class="col-md-8"><div><label>Social Media Link</label><input type="text" class="form-control" required name="social_media_link[]"></div></div><a href="#" class="remove_field">Remove</a></div>'); //add input box
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            })
        });
        // social media

        // education
        $(document).ready(function() {
            var max_fields      = 100; //maximum input boxes allowed
            var wrapper   		= $(".input_fields_education"); //Fields wrapper
            var add_button      = $(".add_education_field"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div><a href="#" class="remove_field">Remove</a><div class="form-group"><label>Institution Name</label><input type="text" class="form-control" required name="institution_name[]"/></div>' +
                        '<div class="form-group"><label>Major</label><input type="text" class="form-control" required name="major[]"/></div>' +
                        '<div class="form-row"><div class="form-group col-md-6"><label>Start Date</label><input type="date" class="form-control" required name="start_date[]"></div>' +
                        ' <div class="form-group col-md-6"><label>End Date</label><input type="date" class="form-control" name="end_date[]"></div><div class="form-group">' +
                        '<div class="form-group"><label>CGP-A</label><input type="number" class="form-control" max="4" required name="cgpa[]"/></div></div>'

                    ); //add input box
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            })
        });
        //education


        // result
        $(document).ready(function() {
            var max_fields      = 10; //maximum input boxes allowed
            var wrapper   		= $(".input_fields_result"); //Fields wrapper
            var add_button      = $(".add_result_field"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div><a href="#" class="remove_field">Remove</a><div class="form-group"><label>Course Code</label><input type="text" class="form-control" name="course_code[]"/></div>' +
                        '<div class="form-group"><label>Course Title</label><input type="text" class="form-control" max="4" name="course_title[]"/></div>' +
                        '<div class="form-group"><label>Grade</label><input type="text" class="form-control" max="4" name="grade[]"/></div>' +
                        '<div class="form-group"><label>Grade Point</label><input type="number" class="form-control" max="4" name="grade_point[]"/></div>' +
                        '</div>'

                    ); //add input box
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            })
        });
        // result

        // experience
        $(document).ready(function() {
            var max_fields      = 10; //maximum input boxes allowed
            var wrapper   		= $(".input_fields_experience"); //Fields wrapper
            var add_button      = $(".add_experience_field"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div><a href="#" class="remove_field">Remove</a><div class="form-row"><div class="form-group col-md-6"><label>Start Date</label><input type="date" required class="form-control" name="experience_start_date[]"></div> <div class="form-group col-md-6"><label>End Date</label><input type="date" class="form-control" name="experience_end_date[]"></div></div>' +
                        '<div class="form-group"><label>Institution Name</label><input type="text" class="form-control" required name="experience_institution_name[]"/></div>' +
                        '<div class="form-group"><label>Designation</label><input type="text" class="form-control" required name="experience_designation[]"/></div>' +
                        '<div class="form-group"><label>About Job</label><input type="text" class="form-control" required name="experience_about_job[]"/></div></div>'

                    ); //add input box
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            })
        });
        //education

        //skill
        $(document).ready(function() {
            var max_fields      = 10; //maximum input boxes allowed
            var wrapper   		= $(".input_fields_skill"); //Fields wrapper
            var add_button      = $(".add_skill_field"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div><a href="#" class="remove_field">Remove</a><div class="form-group"><select class="form-control" required name="skill[]"><option value="html">HTML</option><option value="css">CSS</option><option value="php">PHP</option><option value="js">JavaScript</option><option value="laravel">Laravel</option><option value="vue_js">Vue.js</option><option value="wordpress">WordPress</option></select></div></div>'); //add input box
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            })
        });
        //skill

        // software skill
        $(document).ready(function() {
            var max_fields      = 100; //maximum input boxes allowed
            var wrapper   		= $(".input_fields_software_skill"); //Fields wrapper
            var add_button      = $(".add_software_skill_field"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div><a href="#" class="remove_field">Remove</a><select class="form-control" required name="software_skill[]"><option value="adobe_photoshop">Adobe Photoshop</option><option value="adobe_illustrator">Adobe Illustrator</option><option value="ms_word">MS Word</option><option value="power_point">PowerPoint</option><option value="xampp">XAMPP</option><option value="php_storm">PHP Storm</option><option value="web_storm">Web Storm</option><option value="netbeans">Netbeans</option></select></div>'); //add input box
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            })
        });
        //software skill


        // personality
        $(document).ready(function() {
            var max_fields      = 100; //maximum input boxes allowed
            var wrapper   		= $(".input_fields_personality"); //Fields wrapper
            var add_button      = $(".add_personality_field"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div><a href="#" class="remove_field">Remove</a><input type="text" class="form-control" required name="personality[]"/></div>'); //add input box
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            })
        });
        //personality

        // language
        $(document).ready(function() {
            var max_fields      = 100; //maximum input boxes allowed
            var wrapper   		= $(".input_fields_language"); //Fields wrapper
            var add_button      = $(".add_language_field"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div><a href="#" class="remove_field">Remove</a><div class="form-row"><div class="form-group col-md-6"><label>Language</label><input type="text" class="form-control" required name="language[]"></div><div class="form-group col-md-6"><label>Score</label><select class="form-control" name="score[]"> <option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select></div></div></div>'); //add input box
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            })
        });
        //language


        // hobby

        $(document).ready(function() {
            var max_fields      = 100; //maximum input boxes allowed
            var wrapper   		= $(".input_fields_hobby"); //Fields wrapper
            var add_button      = $(".add_hobby_field"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div><a href="#" class="remove_field">Remove</a><div class="form-group"><input type="text" class="form-control" id="hobby" required name="hobby[]"></div></div>'); //add input box
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            })


            // var hobbies = document.getElementById('hobbies');
            //
            // // For each <li> inside #links
            // for (var i = 0; i < hobbies.length; i++) {
            //     var hobby = hobbies[i];
            //     hobby.onkeyup = function () {
            //         alert('ts')
            //     var inputHobbies = document.getElementById('hobbies').value
            //     document.getElementById('insertHobbies').innerHTML = inputHobbies
            // }
            // }


        });
        //hobby

        // additional section
        $(document).ready(function() {
            var max_fields      = 100; //maximum input boxes allowed
            var wrapper   		= $(".input_fields_additional_section"); //Fields wrapper
            var add_button      = $(".add_additional_section_field"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div><a href="#" class="remove_field">Remove</a><div class="form-group"><label>Subject</label><input type="text" class="form-control" id="subject" required name="subject[]"></div>' +
                        '<div class="form-group"><label>Text</label><input type="text" class="form-control" id="text" required name="text[]"></div></div>'); //add input box
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            })

            var hobbies = document.getElementById('hobbiess')
            hobbies.onkeyup = function() {
                alert('ts')
                var inputHobbies = document.getElementById('hobbies').value
                document.getElementById('insertHobbies').innerHTML = inputHobbies
            }
        });

        //additional section


        ////////////////// submit

        $(document).ready(function(){
            var postURL = "<?php echo url('cv-builder'); ?>";
            var i=1;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $('#submit').click(function(){
                $.ajax({
                    url:postURL,
                    method:"POST",
                    data:$('#add_name').serialize(),
                    //data:new FormData(this),
                    type:'json',
                    // contentType: false,
                    // cache: false,
                    // processData: false,
                    success:function(data)
                    {
                        if(data.error){
                            printErrorMsg(data.error);
                        }else{
                            i=1;
                            $('.dynamic-added').remove();
                            $('#add_name')[0].reset();
                            $(".print-success-msg").find("ul").html('');
                            $(".print-success-msg").css('display','block');
                            $(".print-error-msg").css('display','none');
                            $(".print-success-msg").find("ul").append('<li>Record Inserted Successfully.</li>');
                            window.location = "list-of-cv";
                        }
                    }
                });
            });


            function printErrorMsg (msg) {
                $(".print-error-msg").find("ul").html('');
                $(".print-error-msg").css('display','block');
                $(".print-success-msg").css('display','none');
                $.each( msg, function( key, value ) {
                    $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
                });
            }
        });
    </script>

    @endsection