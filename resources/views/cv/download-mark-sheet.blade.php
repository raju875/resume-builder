<!DOCTYPE html>
<html>
<head>
    <title>Mark Sheet</title>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
            background-color: #dddddd;
        }

    </style>
</head>
<body>
<?php $degrees = \App\Degree_model::where('personal_info_id', $infoId)->get()?>
@foreach($degrees as $degree )
    <h2>{{ $degree->degree }}</h2>
    <?php $results = \App\Result_model::where('degree_id', $degree->id)->where('personal_info_id', $infoId)->get()?>
    <table>
        <tr>
            <th>SL</th>
            <th>Course Code</th>
            <th>Course Title</th>
            <th>Grade</th>
            <th>Grade Point</th>
        </tr>
        @foreach($results as $key => $result )
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $result->course_code }}</td>
                <td>{{ $result->course_title }}</td>
                <td>{{ $result->grade }}</td>
                <td>{{ $result->grade_point }}</td>
            </tr>
        @endforeach
    </table>
@endforeach

</body>
</html>