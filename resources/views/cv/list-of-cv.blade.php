@extends('cv.master')

@section('title')
    <title>List Of CV</title>
    @endsection

@section('content')

    <div class="container mt-5">
        <div class="row">

            <!--  My CV-->
            <div class="coi-lg-12 col-md-12 col-sm-12 col-xs-12">

                <h5 class="text-dark text-center">List Of Resumes</h5>
                @if(Session::has('message'))
                    <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                @endif
                <div class="alert alert-success print-success-msg" style="display:none">
                    <ul></ul>
                </div>
                <table class="table mt-5">
                    <thead>
                    <tr>
                        <th scope="col">SL</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Address</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $personalInfos = \App\PersonalInfo_model::orderBy('id','desc')->get() ?>
                    @foreach($personalInfos as $key => $info )
                    <tr>
                        <th scope="row">{{ $key+1 }}</th>
                        <td>{{ $info->first_name }} {{ $info->last_name }}</td>
                        <td>{{ $info->email }}</td>
                        <td>{{ $info->phone }}</td>
                        <td>{{ $info->address }}<br>{{ $info->zip_code }}</td>
                        <td>
                            <input type="hidden" id="dataId" value="{{ $info->id }}">
                            <button type="button" class="btn btn-outline-info" ><a href="{{ url('/my-cv/'.$info->id) }}" >View</a></button>
                            <button type="button" class="btn btn-outline-primary"><a href="{{ url('/download-resume/'.$info->id) }}" >Download</a></button>
                            <button type="button" class="btn btn-outline-danger deleteRecord" onclick="return confirm('Are you sure to delete it !!!')"><a href="{{ url('/delete-cv/'.$info->id) }}" >Delete</a></button>
                    </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!--  My CV-->
        </div>
    </div>

    @endsection