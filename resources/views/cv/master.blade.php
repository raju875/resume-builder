<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1">
    @yield('title')
    <link rel="stylesheet" href="{{ asset('cv/assets/') }} /bootstrap-4.3.1-dist/css/bootstrap.css"/>
    <link rel="stylesheet" href="{{ asset('cv/assets/') }} /bootstrap-4.3.1-dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="{{ asset('cv/assets/') }} /style.css"/>
    <script src="https://kit.fontawesome.com/5f7b503110.js"></script>
    <!--  <script src="{{ asset('cv/assets/') }} /bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>-->
    <script src="{{ asset('cv/assets/') }} /jquery/jquery-3.4.1.min.js"></script>
    <script src="{{ asset('cv/assets/') }} /bootstrap-4.3.1-dist/js/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="{{ asset('cv/assets/') }} /bootstrap-4.3.1-dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>
@include('cv.include.navbar')
@yield('content')

</body>

</html>