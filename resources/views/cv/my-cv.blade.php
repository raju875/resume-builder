@extends('cv.master')

@section('title')
    <title>CV Builder</title>
    @endsection


@section('content')
    <div class="container">
        <a href=" {{ url('/paf-mark-sheet/'.$infoId) }} " ><button type="button" class="btn btn-secondary">Result/Mark Sheet</button></a>
    </div>
    <div class="container my-container mt-5">
        <div class="row">
            <!--  My CV-->
            <div class="coi-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-3 mt-3">
                        <img src="{{ asset('cv/assets/') }} /images/image.jpg" class="img-fluid" alt="Image" style="width:150px; height: 120px; border-radius:50%">
                    </div>
                    <div class="col-md-9">
                        <?php $personalInfoById = \App\PersonalInfo_model::find($infoId)?>
                        <h5>{{ $personalInfoById->first_name }} {{ $personalInfoById->last_name }}</h5>

                            <?php $profileInfoById = \App\Profile_model::where('personal_info_id',$infoId)->first()?>
                        <h6>{{ $profileInfoById->job_title }}</h6>
                        <p class="text-justify">{{ $profileInfoById->summary }}</p>

                        <!--    Location/Date-of-birth/Phone/Email-->
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6"><i class="fas fa-home"></i>{{ $personalInfoById->address }}, {{ $personalInfoById->zip_code }}</div>
                                <div class="col-md-6"><i class="fas fa-birthday-cake"></i>{{ $personalInfoById->date_of_birth }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6"><i class="fas fa-mobile-alt"></i>{{ $personalInfoById->phone }}</div>
                                <div class="col-md-6"><i class="fas fa-envelope-open-text"></i>{{ $personalInfoById->email }}</div>
                            </div>
                        </div>
                        <!--    Location/Date-of-birth/Phone/Email-->
                    </div>

                    <!--    Social Media-->
                    <div class="container">
                        <h4>Social Media</h4>
                        <hr>
                        <div class="container-fluid">
                            <?php $socialMedias = \App\SocialMedia_model::where('personal_info_id',$infoId)->get()?>
                            @foreach($socialMedias->chunk(2) as $medias)
                            <div class="row">
                                @foreach($medias as $media)
                                    @if($media->social_media == 'facebook')
                                    <div class="col-md-6"><i class="fa fa-facebook"></i> {{ $media->social_media_link }}</div>
                                    @endif
                                        @if($media->social_media == 'linkdin')
                                            <div class="col-md-6"><i class="fab fa-linkedin-in"></i> {{ $media->social_media_link }}</div>
                                        @endif
                                        @if($media->social_media == 'github')
                                            <div class="col-md-6"><i class="fab fa-github"></i>{{ $media->social_media_link }}</div>
                                        @endif
                                        @if($media->social_media == 'skype')
                                            <div class="col-md-6"><i class="fab fa-skype"></i>{{ $media->social_media_link }}</div>
                                        @endif
                                        @if($media->social_media == 'google_plus')
                                            <div class="col-md-6"><i class="fab fa-google-plus-g"></i>{{ $media->social_media_link }}</div>
                                        @endif
                                        @if($media->social_media == 'twitter')
                                            <div class="col-md-6"><i class="fab fa-twitter"></i>{{ $media->social_media_link }}</div>
                                        @endif
                                    @endforeach
                            </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                    <!--    Social Media-->

                    <!--    Education-->
                    <div class="container">
                        <h4>Education</h4>
                        <hr>
                        <div class="container-fluid">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">SL</th>
                                    <th scope="col">Institution Name</th>
                                    <th scope="col">Group</th>
                                    <th scope="col">CGP-A</th>
                                    <th scope="col">Passing Year</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php  $educations = \App\Education_model::where('personal_info_id',$infoId)->get() ?>
                                @foreach($educations as $key => $education )
                                <tr>
                                    <th scope="row">{{ $key+1 }}</th>
                                    <td>{{ $education->institution_name }}</td>
                                    <td>{{ $education->major }}</td>
                                    <td>{{ $education->cgpa }}</td>
                                    <td>{{ $education->start_date }} - @if(empty($education->end_date)){{ 'present' }} @else {{ $education->end_date }} @endif </td>

                                </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--    Education-->

                    <!--    Experience-->
                <?php $experiences = \App\Experience_model::where('personal_info_id',$infoId)->get()?>
                @foreach($experiences as $experience )
                    @if(!empty($experience->start_date))
                    <div class="container">
                        <h4>Experience</h4>
                        <hr>
                        @foreach($experiences as $exp )
                                @if(!empty($exp->start_date))
                        <div class="container-fluid">
                            <h5>{{ $exp->experience_institution_name }}</h5>
                            <h6>{{ $exp->experience_designation }}</h6>
                            <h6>{{ $exp->experience_start_date }} - @if(empty($exp->experience_end_date)){{ 'present' }} @else {{ $exp->experience_end_date }} @endif </h6>
                            <h6>{{ $exp->experience_about_job }}</h6>
                        </div>
                                    <br>
                                @else
                                @endif
                            @endforeach
                    </div>
                        @break
                @else
                    @break
                @endif
            @endforeach
                    <!--    Experience-->


                    <div class="container">
                        <div class="row">
                            <!--     Skill-->
                            <div class="col-md-6">
                                <div class="container">
                                    <h2>Skill</h2>
                                    <hr>
                                    <?php $skills = \App\Skill_model::where('personal_info_id',$infoId)->get()?>
                                    <ul class="list-group">
                                        @foreach($skills as $skill )
                                        <li>{{ $skill->skill }}</li>
                                            @endforeach
                                    </ul>
                                </div>
                            </div>
                            <!--     Skill-->

                            <!--    Software Skill-->
                            <div class="col-md-6">
                                <div class="container">
                                    <h2>Software Skill</h2>
                                    <hr>
                                    <?php $softwareSkills = \App\SoftwareSkill_model::where('personal_info_id',$infoId)->get()?>
                                    <ul class="list-group">
                                        @foreach($softwareSkills as $software )
                                        <li>{{ $software->software_skill }}</li>
                                            @endforeach
                                    </ul>
                                </div>
                            </div>
                            <!--    Software Skill-->
                        </div>
                    </div>

                    <div class="container">
                        <div class="row">
                            <!--     Personality-->
                            <div class="col-md-6">
                                <div class="container">
                                    <h2>Personality</h2>
                                    <hr>
                                    <?php $personalities = \App\Personality_model::where('personal_info_id',$infoId)->get()?>
                                    @foreach($personalities->chunk(4) as $personality )
                                        <div class="row">
                                        @foreach($personality as $personality )
                                        <div class="col-md-3">
                                            <div class="badge badge-secondary text-wrap">{{ $personality->personality }}</div>
                                        </div>
                                        @endforeach
                                        </div>
                                        @endforeach
                                </div>
                            </div>
                            <!--     Personality-->

                            <!--    Language-->
                            <div class="col-md-6">
                                <div class="container">
                                    <h2>Language</h2>
                                    <hr>
                                    <?php $languages = \App\Language_model::where('personal_info_id',$infoId)->get()?>
                                    @foreach($languages as $language )
                                        <p>{{ $language->language }}</p>
                                        @if($language->score == 5 )
                                            <span class="dotDark"></span>
                                            <span class="dotDark"></span>
                                            <span class="dotDark"></span>
                                            <span class="dotDark"></span>
                                            <span class="dotDark"></span>
                                        @elseif($language->score == 4 )
                                            <span class="dotDark"></span>
                                            <span class="dotDark"></span>
                                            <span class="dotDark"></span>
                                            <span class="dotDark"></span>
                                            <span class="dot"></span>
                                        @elseif($language->score == 3 )
                                            <span class="dotDark"></span>
                                            <span class="dotDark"></span>
                                            <span class="dotDark"></span>
                                            <span class="dot"></span>
                                            <span class="dot"></span>
                                        @elseif($language->score == 2 )
                                            <span class="dotDark"></span>
                                            <span class="dotDark"></span>
                                            <span class="dot"></span>
                                            <span class="dot"></span>
                                            <span class="dot"></span>
                                        @elseif($language->score == 1 )
                                            <span class="dotDark"></span>
                                            <span class="dot"></span>
                                            <span class="dot"></span>
                                            <span class="dot"></span>
                                            <span class="dot"></span>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <!--    Language-->
                        </div>
                    </div>


                <!--    Additional Section-->
                <?php $additionalSections = \App\AdditionalSection_model::where('personal_info_id',$infoId)->get()?>
                @foreach($additionalSections as $additional )
                    @if(!empty($additional->subject))
                        <div class="container">
                            <h4>Additional Section</h4>
                            <hr>
                            @foreach($additionalSections as $key => $section )
                                @if(!empty($section->subject))
                                    <div class="container-fluid">
                                        <div class="row">{{ $key+1 }}. <h5>{{   $section->subject }}</h5></div>
                                        <p>{{ $section->text }}</p>
                                    </div>
                                @else
                                @endif
                            @endforeach
                        </div>
                        @break
                    @else
                        @break
                    @endif
                @endforeach
                <!--    Additional Section-->

                <!--    Experience-->
                <?php $experiences = \App\Experience_model::where('personal_info_id',$infoId)->get()?>
                @foreach($experiences as $experience )
                    @if(!empty($experience->start_date))
                        <div class="container">
                            <h4>Experience</h4>
                            @break
                            <hr>
                            @foreach($experiences as $experience )
                                @if(!empty($experience->start_date))
                                    <div class="container-fluid">
                                        <h5>{{ $experience->experience_institution_name }}</h5>
                                        <h6>{{ $experience->experience_designation }}</h6>
                                        <h6>{{ $experience->experience_start_date }} - @if(empty($experience->experience_end_date)){{ 'present' }} @else {{ $experience->experience_end_date }} @endif </h6>
                                        <h6>{{ $experience->experience_about_job }}</h6>
                                    </div>
                                @else
                                @endif
                            @endforeach
                        </div>
                @else
                    @break
                @endif
            @endforeach
            <!--    Experience-->

                    <!--    Hobby-->
                    <div class="container">
                        <div class="col-12">
                            <div class="container">
                                <h2>Hobby</h2>
                                <hr>
                                <?php $hobbies = \App\Hobby_model::where('personal_info_id',$infoId)->get()?>
                                @foreach($hobbies->chunk(3) as $hobbies )
                                    @foreach($hobbies as $hobby )
                                            <button type="button" class="btn btn-light">{{ $hobby->hobby }}</button>
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!--    Hobby-->
                </div>
            </div>
            <!--  My CV-->
        </div>
    </div>
    @endsection