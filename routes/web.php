<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get("addmore","CVBuilderController@addMore");
Route::post("addmore","CVBuilderController@addMorePost");


Route::get("/","CVBuilderController@cvBuilder");
Route::get("/cv-builder","CVBuilderController@cvBuilder");
Route::post("cv-builder","CVBuilderController@cvBuilderPost");
Route::get("/list-of-cv","CVBuilderController@cvList");
Route::get("/my-cv/{id}","CVBuilderController@myCv");
Route::get("/download-resume/{id}","CVBuilderController@resumeDownload");
Route::get("/delete-cv/{id}","CVBuilderController@resumeDelete");
Route::get("/paf-mark-sheet/{id}","CVBuilderController@markSheetDownload");
